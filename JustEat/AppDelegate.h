//
//  AppDelegate.h
//  JustEat
//
//  Created by Liam Nichols on 20/10/2014.
//  Copyright (c) 2014 Liam Nichols. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

